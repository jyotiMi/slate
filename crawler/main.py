from requests import Session
from bs4 import BeautifulSoup
import json

s = Session()

s.headers.update({
    "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
})

bsObj = BeautifulSoup(s.get('https://www.fusioncharts.com/dev/chart-attributes/errorline').text, 'lxml')

lis = []
for ul in bsObj.findAll('ul', attrs={'class': 'list-unstyled'}):
    lis += [{'title': li.text, 'chartType': li.attrs['data-alias']} for li in ul.findAll('span') if li.has_attr('data-alias')]

for li in lis:
    jsonFile = open('../src/assets/json/'+ li['chartType'] + '.json', 'wb+')
    jsonData = s.get('https://www.fusioncharts.com/dev/portal/attribute/'+li['chartType']).text
    jsonFile.write(jsonData.encode('utf-8'))
    jsonFile.close()