import json
import re

dir = '../src/assets/json/'
files = ['column2d.json', 'area2d.json']
SPLT = re.compile("_|\"|'")

def findDescription(attrName, chartType='column2d'):
    doc = open(dir + chartType + '.json', 'rb')
    jsonDoc = json.loads(doc.read())
    for table in jsonDoc:
        for attr in table["attrs"]:
            if attr["name"].lower() == attrName.lower():
                return attr["description"]
    doc.close()

fp = open("../src/js/aside/asideData.js", "r")
nfp = open("../src/js/aside/asideDataDesc.js", "w+")
code = fp.readlines()
newCode = []
findStr = ["\"id\":", "'id':", "id:"]
for line in code:
    idStart = max([line.find(s) for s in findStr])
    if(idStart >= 0):
        attrName = line.split("_")[1].replace("\"", "").replace("'", "").replace(",", "").strip()
        desc = findDescription(attrName)
        if desc:
            newCode.append(''.join([' ' for e in range(0, idStart)]) + "\"description\":\"" + desc.replace("\n", "").replace("\"", "&#039#;") + "\",\r\n")
    newCode.append(line)
nfp.writelines(newCode)
fp.close()
nfp.close()

