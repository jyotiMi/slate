const path = require('path');

let fileName = 'slate',
	mode = process.argv[2].slice(2);

if (mode === 'prod') {
	fileName += '.min.js';
	mode = 'production';
} else {
	fileName += '.js';
	mode = 'development';
}

module.exports = {
    entry: [ '@babel/polyfill', './src/js/index.js'],
    output: {
        filename: fileName,
        path: path.resolve(__dirname, './dist/js'),
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader',
            query: {
                presets: ['@babel/preset-env']
            }
        }]
    },
    mode: mode
};