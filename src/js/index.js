import {chartObject, chart, createChart, setChart, setChartObject} from './createchart.js';
import {snackbar} from './snackbar'
import {dataSource} from './dataSource'
import {filterLink} from './filterLink'
import {functionWrapper} from './aside/accordian'
import {editJson} from './aside/edit_json'
import {exportHTML} from './aside/exportHTML';
window.openJSONEditor = function () {
  const asideMenu = document.getElementById('aside-menu-wraper');
  const workspace = document.getElementById('workspace');
  const btnAside = document.getElementById('btn-open-aside');
  const btnJSON = document.getElementById('btn-open-json');
  const chartContainer = document.getElementById('chart-container');
  const needResize = document.getElementById('chart-width').value.trim() === '' || document.getElementById('chart-height').value.trim() === '';

  if (needResize) {
    chartContainer.style.cssText = 'height:' + chartContainer.offsetHeight + 'px;width:' + chartContainer.offsetWidth + 'px;opacity:0;';
    setTimeout(() => {
      chartContainer.style.cssText = 'opacity:0';
    }, 300);
    setTimeout(() => {
      chartContainer.removeAttribute('style');
    }, 600);
  }

  if (chart) {
    btnJSON.setAttribute('onclick', 'closeAsideMenu()');
    btnJSON.innerHTML = '<span class="fa fa-remove"></span>';
    btnAside.setAttribute('onclick', 'openAsideMenu()');
    btnAside.innerHTML = '<span class="fa fa-sliders"></span>';
    workspace.classList.remove('full');
    workspace.classList.add('half');
    asideMenu.classList.add('half');
    asideMenu.style.display = 'inline-block';
    const json = document.getElementById('json');
    const accordian = document.getElementById('aside-customize');

    if (json) {
      json.removeAttribute('style');
      document.getElementById('json-textarea').value = JSON.stringify(chartObject, null, 2);
    } else {
      editJson();
    }

    if (accordian) {
      accordian.style.cssText = 'display:none;';
    }
  } else {
    snackbar.showSnackbar('No chart rendered yet');
  }
}

window.openAsideMenu = function () {
  const asideMenu = document.getElementById('aside-menu-wraper');
  const workspace = document.getElementById('workspace');
  const btnAside = document.getElementById('btn-open-aside');
  const btnJSON = document.getElementById('btn-open-json');
  const chartContainer = document.getElementById('chart-container');
  const needResize = document.getElementById('chart-width').value.trim() === '' || document.getElementById('chart-height').value.trim() === '';

  if (needResize) {
    chartContainer.style.cssText = 'height:' + chartContainer.offsetHeight + 'px;width:' + chartContainer.offsetWidth + 'px;opacity:0;';
    setTimeout(() => {
      chartContainer.style.cssText = 'opacity:0';
    }, 300);
    setTimeout(() => {
      chartContainer.removeAttribute('style');
    }, 600);
  }

  if (chart) {
    btnJSON.setAttribute('onclick', 'openJSONEditor()');
    btnJSON.innerHTML = '<span class="fa fa-code"></span>';
    btnAside.setAttribute('onclick', 'closeAsideMenu()');
    btnAside.innerHTML = '<span class="fa fa-remove"></span>';
    workspace.classList.remove('full');
    workspace.classList.remove('half');
    asideMenu.classList.remove('half');
    const json = document.getElementById('json');
    const accordian = document.getElementById('aside-customize');

    if (json) {
      json.style.display = 'none';
    }

    if (accordian) {
      accordian.removeAttribute('style');
    }

    asideMenu.style.display = 'inline-block';
  } else {
    snackbar.showSnackbar('No chart rendered yet');
  }
}

window.closeAsideMenu = function () {
  const asideMenu = document.getElementById('aside-menu-wraper');
  const workspace = document.getElementById('workspace');
  const btnAside = document.getElementById('btn-open-aside');
  const btnJSON = document.getElementById('btn-open-json');
  const chartContainer = document.getElementById('chart-container');
  const needResize = document.getElementById('chart-width').value.trim() === '' || document.getElementById('chart-height').value.trim() === '';

  if (needResize) {
    chartContainer.style.cssText = 'height:' + chartContainer.offsetHeight + 'px;width:' + chartContainer.offsetWidth + 'px;opacity:0;';
    setTimeout(() => {
      chartContainer.style.cssText = 'opacity:0';
    }, 300);
    setTimeout(() => {
      chartContainer.removeAttribute('style');
    }, 600);
  }

  btnJSON.setAttribute('onclick', 'openJSONEditor()');
  btnJSON.innerHTML = '<span class="fa fa-code"></span>';
  btnAside.setAttribute('onclick', 'openAsideMenu()');
  btnAside.innerHTML = '<span class="fa fa-sliders"></span>';
  workspace.classList.remove('half');
  workspace.classList.add('full');
  asideMenu.style.display = 'none'; // close all accordion dropdowns as well

  const dom = document.getElementsByClassName('dropdown');

  for (let index = 0; index < dom.length; index++) {
    dom[index].style.display = 'none';
  } // clear parentEvent


  functionWrapper.resetGlobal();
}

window.modalOpen = function (elemId) {
  const modalContainer = document.getElementById('modal-container');
  const modal = document.getElementById(elemId);
  const workspace = document.getElementById('workspace-container');
  modalContainer.style.display = 'block';
  workspace.style.opacity = '0';
  modalContainer.classList.add('opened');
  setTimeout(function () {
    modal.classList.add('opened');
  }, 200);
}

window.modalClose = function (elemId) {
  const modalContainer = document.getElementById('modal-container');
  const modal = document.getElementById(elemId);
  const workspace = document.getElementById('workspace-container');
  modalContainer.classList.remove('opened');
  workspace.style.opacity = '1';
  setTimeout(function () {
    modalContainer.style.display = 'none';
  }, 300);
  modal.classList.remove('opened');
}

window.initChartSelection = function () {
  const select = document.getElementById('chart-type');
  const chartTypes = ['line', 'area2d', 'pie2d', 'pie3d', 'column2d', 'column3d', 'bar2d', 'bar3d', 'mscolumn2d', 'mscolumn3d', 'msbar2d', 'msbar3d'];

  for (const chartType of chartTypes) {
    const option = document.createElement('option');
    option.value = chartType;
    option.innerHTML = chartType;
    select.appendChild(option);
  }

  select.onchange = function () {
    const btn = document.getElementById('btn-spreadsheet-modal');
    btn.removeAttribute('onClick');
    btn.onclick = () => {
      dataSource.canHaveJson();
      window.modalClose('modal-spreadsheet');
    }
    if (dataSource.type && dataSource.type.indexOf('ms') >= 0) {
      if (select.value.indexOf('ms') === -1) {
        dataSource.cleanSelection();
      }
    }

    dataSource.type = select.value; // dataSource.canHaveJson(0, [1]);
  };
}

window.clearSession = function () {
  localStorage.removeItem('slateDataSource');
  localStorage.removeItem('slateChartObject');
  window.location.reload();
}

window.restoreSession = function () {
  let oldDataSource = localStorage.getItem('slateDataSource');
  const oldChartObject = localStorage.getItem('slateChartObject');
  const select = document.getElementById('chart-type');
  const btn = document.getElementById('btn-spreadsheet-modal');
  const skipList = ['LINE_SEPERATOR', 'COL_SEPERATOR'];

  if (oldDataSource) {
    oldDataSource = JSON.parse(oldDataSource);
    Object.keys(oldDataSource).forEach(key => {
      if (!skipList.includes(key)) {
        dataSource[key] = oldDataSource[key];
      }
    });
    select.value = dataSource.type;
    btn.className = 'btn btn-done'; // btn.innerHTML = '<span class="fa fa-bar-chart"></span>';

    btn.setAttribute('onClick', 'javascript:dataSource.canHaveJson();modalClose(\'modal-spreadsheet\');');
    document.getElementById('chart-type').removeAttribute('style');
    dataSource.showPreview();
    dataSource.canHaveJson();

    if (oldChartObject) {
      setChartObject(JSON.parse(oldChartObject));
      chart.setJSONData(filterLink());
    }

    document.getElementById('txt-file-name').innerHTML = dataSource.fileName || 'No File Chosen';
  }
}

window.init = function () {
  const workspaceContainer = document.getElementById('workspace-container');
  workspaceContainer.style.cssText = 'height:' + (window.innerHeight - document.getElementById('header').offsetHeight) + 'px';
  document.getElementById('aside-menu').style.cssText = 'height:' + (window.innerHeight - document.getElementById('header').offsetHeight) + 'px';
}

window.setChartContainerSize = function (width, height) {
  if (height === '100%') {
    document.getElementById('chart-width').value = '';
    document.getElementById('chart-height').value = '';
    document.getElementById('chart-container').style.cssText = 'height:' + (window.innerHeight - document.getElementById('header').offsetHeight - 70) + 'px';
  } else {
    document.getElementById('chart-container').removeAttribute('style');
  }

  if (chart) {
    chart.resizeTo(width, height);
  }
}

window.start = (function () {
  document.getElementById('fileUpload').onchange = function (e) {
    dataSource.cleanSelection();
    dataSource.cleanData();
    document.getElementById('txt-file-name').innerHTML = this.files[0] ? this.files[0].name : dataSource.fileName;
    const reader = new FileReader();

    reader.onload = function (evt) {
      dataSource.init(reader.result);
    };

    reader.readAsText(this.files[0]);
    dataSource.fileName = this.files[0].name;
  };

  document.getElementById('chart-width').addEventListener('keyup', function () {
    setChartContainerSize(document.getElementById('chart-width').value, document.getElementById('chart-height').value);
  });
  document.getElementById('chart-height').addEventListener('keyup', function () {
    setChartContainerSize(document.getElementById('chart-width').value, document.getElementById('chart-height').value);
  });

  document.getElementById('btn-export-html').addEventListener('click', function(e){
    exportHTML(e);
  });

  snackbar.init();

  window.onload = function () {
    init();
    initChartSelection();
    if(localStorage){
      restoreSession();
    }
  };

  window.onresize = function () {
    init();
  };
})();