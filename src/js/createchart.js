import {extension_renderComplete} from './aside/handleClicks';
import {functionWrapper} from './aside/accordian';
var chart;
var chartObject;
function setChartObject(value) {
  chartObject = value
}
function setChart(value) {
  chart = value
}
function createChart(chartType) {
  FusionCharts.addDep(extension_renderComplete);
  chart = new FusionCharts({
    'type': chartType,
    'width': '100%',
    'height': '100%',
    'renderAt': 'chart-container',
    'dataFormat': 'json',
    'events': {
      'dataPlotClick': function(e) {
        const event = {
          'name': 'dataplot',
          'dataIndex': e.data.dataIndex,
          'datasetIndex': e.data.datasetIndex,
        };
        functionWrapper.handleClickType(event);
      },
      'rendered': function(e, d) {
        chartObject = chart.getJSONData();
        localStorage.setItem('slateChartObject', JSON.stringify(chartObject));
        functionWrapper.createAccordion();
      },
      'chartTypeChanged': function(e, d) {
        functionWrapper.createAccordion();
        functionWrapper.handleClickType({
          'name': 'dataplotclick',
          'datasetIndex': e.data.datasetIndex,
          'dataIndex': e.data.dataIndex,
        });
      },
      'legendItemClicked': function(e) {
        functionWrapper.handleClickType({
          'name': 'legend',
          'dataIndex': e.data.dataIndex,
        });
      },
      'renderComplete': function(e, d) {
        chartObject = chart.getJSONData();
        const jsonText = document.getElementById('json-textarea');
        localStorage.setItem('slateChartObject', JSON.stringify(chartObject));
        if (jsonText) {
          // jsonText.value = JSON.stringify(chartObject, null, 2);
        }
      },
    },
  }).render();
}
export {
  chart,
  chartObject,
  createChart,
  setChart,
  setChartObject
}
