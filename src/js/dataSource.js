/*eslint-disable*/ 
import {chartObject, chart, createChart, setChart, setChartObject} from './createchart.js';
import {filterLink} from './filterLink.js';
import {snackbar} from './snackbar'

var dataSource = {
  LINE_SEPERATOR: '\n',
  COL_SEPERATOR: /,(?=(?:(?:[^"]*"){2})*[^"]*$)/,
  dataformat: 'json',
  axis: [],
  selectedY: [],
  init: function(datacsv) {
    this.dataTable = [];
    this.headers = [];
    this.axis = [];
    datacsv = datacsv.trim();
    const lines = datacsv.split(this.LINE_SEPERATOR);
    this.headers = lines[0].split(this.COL_SEPERATOR);
    for (let i = 1; i < lines.length; i++) {
      this.dataTable.push(lines[i].split(this.COL_SEPERATOR));
    }
    const btn = document.getElementById('btn-spreadsheet-modal');
    btn.className = 'btn btn-done';
    
    btn.onclick = () => {
      this.canHaveJson();
      window.modalClose('modal-spreadsheet');
    }

    // btn.setAttribute('onClick', 'javascript:dataSource.canHaveJson();modalClose(\'modal-spreadsheet\');');
    document.getElementById('chart-type').removeAttribute('style');
    this.showPreview();
  },
  canHaveJson: function(selectedX = this.selectedX, selectedY = this.selectedY) {
    this.axis = [selectedX, ...selectedY];
    const btn = document.getElementById('btn-spreadsheet-modal');
    btn.setAttribute('onClick', 'modalClose(\'modal-spreadsheet\')');

    if (!this.type || this.type === '') {
      snackbar.showSnackbar('Select a chart Type');
    }
    if (!chart) {
      createChart(this.type);
    }
    if (selectedY == null || selectedY.length === 0) {
      snackbar.showSnackbar('select some columns in y axis');
    }
   
    if ((this.type.indexOf('ms') != -1) && (selectedY.length >= 1)) {
      this.csvJSONMultiSeries();
    } else {
      this.csvJSONSingleSeries();
    }
    // }
  },
  csvJSONSingleSeries: function() {
    const result = [];
    for (let i = 0; i < this.dataTable.length; i++) {
      const obj = {};
      for (let j = 0; j < this.axis.length; j++) {
        if (j == 0) {
          obj['label'] = this.dataTable[i][this.axis[j]];
        } else {
          obj['value'] = this.dataTable[i][this.axis[j]];
        }
      }
      result.push(obj);
    }
    
    this.json = JSON.parse(JSON.stringify(result)); // JSON

    this.createDataSource();
  },
  csvJSONMultiSeries: function() {
    this.categories = [], this.json = [];
    const category = [];
    let data = [];
    let obj, i;

    for (i = 0; i < this.dataTable.length; i++) {
      obj = {};

      obj['label'] = this.dataTable[i][this.axis[0]];
      category.push(obj);
    }
    this.categories.push({
      'category': category,
    });

    for (let j = 1; j < this.axis.length; j++) {
      data = [];
      for (i = 0; i < this.dataTable.length; i++) {
        obj = {};

        obj['value'] = this.dataTable[i][this.axis[j]];
        data.push(obj);
      }
      this.json.push({
        'seriesname': this.headers[this.axis[j]],
        'data': data,
      });
    }
    this.createDataSource();
  },
  csvJSONXYcharts: function() {
    // for xy charts
    this.categories = [], this.json = [];
    const category = [];
    let data = [];
    let obj;

    for (i = 0; i < this.dataTable.length; i++) {
      obj = {};
      ///not yet decided where to put labels on which X positions
      obj['label'] = this.dataTable[i][this.axis[0]];
      category.push(obj);
    }
    this.categories.push({
      'category': category,
    });

    for (let j = 1; j < this.axis.length; j++) {
      data = [];
      for (i = 0; i < this.dataTable.length; i++) {
        obj = {};

        obj['value'] = this.dataTable[i][this.axis[j]];
        data.push(obj);
      }
      this.json.push({
        'seriesname': this.headers[this.axis[j]],
        'data': data,
      });
    }
    this.createDataSource();
  },
  createDataSource: function() {
    const dataset = this.json;
    var datasource
    let chartobj = {
      'caption': 'click to add caption',
      'subcaption': 'click to add subcaption',
      'xaxisname': 'Enter X axis name',
      'yaxisname': 'Enter Y axis name',
    };

    if (chartObject) {
      chartobj = {
        ...chartobj,
        ...chartObject['chart'],
      };
    }
    if (this.type.indexOf('ms') == -1) {
      datasource = {
        'chart': chartobj,
        'data': dataset,
      };
    } else {
      datasource = {
        'chart': chartobj,
        'categories': this.categories,
        'dataset': dataset,
      };
    }
    chart.chartType(this.type);
    setChartObject(datasource);

    const jsonTextArea = document.getElementById('json-textarea');
    if (jsonTextArea) {
      jsonTextArea.value = JSON.stringify(chartObject, null, 2);
    }

    if(localStorage){
      localStorage.setItem('slateDataSource', JSON.stringify(dataSource));
    }

    chart.setJSONData(filterLink());
  },
  _fillTable: function(tbl) {
    let tr; let td; let cellTag = 'th';
    const dictIdx = [];
    const pallets = 3;
    const selectedX = this.axis[0];
    const selectedY = [...this.axis.slice(1)];
    tbl.innerHTML = '';

    tr = document.createElement('tr');
    td = document.createElement(cellTag);

    td.innerHTML = 'SL';
    tr.appendChild(td);
    this.headers.forEach((col, colIdx) => {
      td = document.createElement(cellTag);
      td.setAttribute('onclick', 'selectAxisMenu(event, ' + colIdx + ')');
      td.setAttribute('oncontextmenu', 'selectAxisMenu(event, ' + colIdx + ')');

      if (selectedX === colIdx) {
        td.className = 'selected-x';
      }
      if (selectedY.includes(colIdx)) {
        td.className = 'selected-y';
      }
      td.innerHTML = col;
      tr.appendChild(td);
    });
    tbl.appendChild(tr);

    cellTag = 'td';
    this.dataTable.forEach((line, lineNo) => {
      tr = document.createElement('tr');

      td = document.createElement(cellTag);
      td.className = 'row-idx';
      td.innerHTML = lineNo + 1;

      tr.appendChild(td);

      line.forEach((col, colIdx) => {
        td = document.createElement(cellTag);
        td.setAttribute('onclick', 'selectAxisMenu(event, ' + colIdx + ')');
        td.setAttribute('oncontextmenu', 'selectAxisMenu(event, ' + colIdx + ')');
        td.innerHTML = col;
        tr.appendChild(td);
      });
      tbl.appendChild(tr);
    });
  },
  showPreview: function() {
    const tbl = document.getElementById('table-csv');
    this._fillTable(tbl);

    document.getElementById('table-container').addEventListener('click', function(e) {
      e.stopPropagation();
    });
    document.getElementById('table-container').addEventListener('contextmenu', function(e) {
      e.preventDefault();
      e.stopPropagation();
    });
    document.getElementById('table-container').style.cssText = 'height:' + (window.innerHeight - document.getElementById('modal-nav').offsetHeight - document.getElementById('header').offsetHeight - 70) + 'px;';
  },
  cleanSelection() {
    let toSelect = document.getElementById('table-csv').querySelector('tr:first-child th.selected-x');
    if (toSelect) {
      toSelect.classList.remove('selected-x');
    }
    toSelect = document.getElementById('table-csv').querySelectorAll('tr:first-child th.selected-y');
    if (toSelect) {
      Array.from(toSelect).forEach((e) => e.classList.remove('selected-y'));
    }
    dataSource.selectedY = [];
    dataSource.selectedX = undefined;
    dataSource.axis = [];
  },
  cleanData() {
    dataSource.dataTable = [];
    dataSource.headers = [];
  },
};

window.hideSelectAxisMenu =  function () {
  const menu = document.getElementById('select-axis-menu');

  if (menu) {
    // menu.classList.add('hidden');
    menu.style.display = 'none';
    window.removeEventListener('onclick', hideSelectAxisMenu);
  }
}

window.createSelectAxisMenu =  function (options, callback) {
  const menu = document.createElement('div');
  let btn;

  menu.id = 'select-axis-menu';
  menu.className = 'select-axis-menu';
  options.forEach((option) => {
    btn = document.createElement('a');
    btn.href = '#!';
    btn.innerHTML = option;
    btn.onclick = function() {
      callback(option);
    };
    menu.appendChild(btn);
  });
  document.body.appendChild(menu);
  return menu;
}

window.updateMenuOptions =  function (menu, options, callback) {
  menu.innerHTML = '';
  options.forEach((option) => {
    let btn = document.createElement('a');
    btn.href = '#!';
    btn.innerHTML = option;
    btn.onclick = function() {
      callback(option);
    };
    menu.appendChild(btn);
  });
}

window.handleSelectAxis =  function (val) {
  if (val.indexOf('X') >= 0) {
    const toSelect = document.getElementById('table-csv').querySelector('tr:first-child th:nth-child(' + (dataSource.currentlySelected + 2) + ')');
    toSelect.classList.toggle('selected-x');
    const btn = document.getElementById('btn-spreadsheet-modal');
    // btn.setAttribute('onClick', 'javascript:dataSource.canHaveJson();modalClose(\'modal-spreadsheet\');');
    btn.onclick = () => {
      dataSource.canHaveJson();
      window.modalClose('modal-spreadsheet');
    }
    if (dataSource.selectedX === dataSource.currentlySelected) {
      dataSource.selectedX = undefined;
    } else {
      if (dataSource.selectedX !== undefined) {
        // previously selected
        document.getElementById('table-csv').querySelector('tr:first-child th:nth-child(' + (dataSource.selectedX + 2) + ')').classList.remove('selected-x');
      }
      dataSource.selectedX = dataSource.currentlySelected;
    }
  } else if (val.indexOf('Y') >= 0) {
    const btn = document.getElementById('btn-spreadsheet-modal');
    // btn.setAttribute('onClick', 'javascript:dataSource.canHaveJson();modalClose(\'modal-spreadsheet\');');
    btn.removeAttribute('onClick');
    btn.onclick = () => {
      dataSource.canHaveJson();
      window.modalClose('modal-spreadsheet');
    }
    const found = dataSource.selectedY.indexOf(dataSource.currentlySelected);
    const toSelect = document.getElementById('table-csv').querySelector('tr:first-child th:nth-child(' + (dataSource.currentlySelected + 2) + ')');
    toSelect.classList.toggle('selected-y');
    if (dataSource.type.indexOf('ms') == -1) {
      if (found >= 0) {
        dataSource.selectedY = [];
      } else {
        dataSource.selectedY.forEach((colIdx) => {
          const toSelect = document.getElementById('table-csv').querySelector('tr:first-child th:nth-child(' + (colIdx + 2) + ')');
          toSelect.classList.toggle('selected-y');
        });

        dataSource.selectedY = [dataSource.currentlySelected];
      }
      // console.log("check")
    } else {
      if (found >= 0) {
        dataSource.selectedY.splice(found, 1);
      } else {
        dataSource.selectedY.push(dataSource.currentlySelected);
      }
    }
  }
}

window.selectAxisMenu = function (e, colIdx) {
  let menu = document.getElementById('select-axis-menu');
  let options = ['Select as X axis', 'Select as Y axis'];
  dataSource.currentlySelected = colIdx;
  if (dataSource.selectedX === colIdx) {
    options[0] = 'Deselect as X axis';
    options = options.slice(0, 1);
  }
  if (dataSource.selectedY.includes(colIdx)) {
    options[0] = 'Deselect as Y axis';
    options = options.slice(0, 1);
  }
  if (dataSource.type) {
    if (!menu) {
      menu = createSelectAxisMenu(options, (val) => {
        handleSelectAxis(val);
      });
    } else {
      updateMenuOptions(menu, options, (val) => {
        handleSelectAxis(val);
      });
    }
    menu.style.cssText = 'left:' + (e.clientX - 30) + 'px;top:' + (e.clientY + 10) + 'px';
    window.addEventListener('click', hideSelectAxisMenu);
  } else {
    snackbar.showSnackbar('Select chart type first');
  }
}

export {dataSource}
