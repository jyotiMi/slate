export const snackbar = {
  elem: document.getElementById('snackbar'),
  snackbarMsg: document.getElementById('snackbar-msg'),
  init: function() {
    if (!this.elem) {
      this.elem = document.createElement('div');
      this.elem.id = 'snackbar';
      this.snackbarMsg = document.createElement('div');
      this.snackbarMsg.id = 'snackbar-msg';
      this.elem.className = 'snackbar';
      this.snackbarMsg.className = 'snackbar-msg';
      this.elem.appendChild(this.snackbarMsg);
      document.body.appendChild(this.elem);
    }
  },
  showSnackbar: function(msg, isSuccess) {
    if (!this.elem) {
      this.init();
    }
    if (!this.requestedSnackbar) {
      requestAnimationFrame(() => {
        this.snackbarMsg.innerHTML = msg;
        this.elem.classList.remove('snackbar-show', 'success');
        requestAnimationFrame(() => {
          this.requestedSnackbar = false;
          this.elem.classList.add('snackbar-show');
          if (isSuccess) {
            this.elem.classList.add('success');
          }
        });
      });
      this.requestedSnackbar = true;
    }
  },
};
