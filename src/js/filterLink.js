/*eslint-disable*/
import {chartObject, chart} from './createchart';
export function filterLink() {
  let modifiedObject = JSON.stringify(chartObject);
  try {
    modifiedObject = JSON.parse(modifiedObject);
    if (modifiedObject['chart']['clickURL']) {
      delete modifiedObject['chart']['clickURL'];
    }
  }catch(e){}

  function checkType() {
    const type = chart.chartType();
    const types = {
      'singleseries': ['column2d', 'area2d', 'line', 'bar2d'],
      'mutliseries': ['mscolumn2d'],
    };
    for (const t in types) {
      for (const series in types[t]) {
        if (series === type) {
          return series;
        }
      }
    }
  }
  const dataSeries = checkType();
  if (dataSeries === 'singleseries') {
    for (const value of modifiedObject['data']) {
      if (value['link']) {
        delete value['link'];
      }
    }
  } else if (dataSeries === 'multiseries') {
    for (const depth1 of modifiedObject['dataset']) {
      for (const depth2 of depth1['data']) {
        if (depth2['link']) {
          delete depth2['link'];
        }
      }
    }
  }
  return modifiedObject;
}
