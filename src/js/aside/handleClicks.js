import {functionWrapper} from './accordian'
export const extension_renderComplete = {
  extension: function(Fusioncharts) {
    const cartesians=['column2d', 'area2d', 'column3d', 'line', 'msline', 'bar2d', 'bar3d', 'mscolumn2d',
      'mscolumn3d', 'msbar2d', 'msbar3d', 'msarea'];
    Fusioncharts.addEventListener('renderComplete', (event) => {
      const chart = event.sender.apiInstance;
      const chartInstance = event.sender;
      if (cartesians.includes(chartInstance.chartType())) {
        // for xAxis labels
        chart._components.xAxis[0].config.categoryLabel.forEach((category, categoryIndex) => {
          if (!chart._components.xAxis[0].config.categoryLabel[categoryIndex]._handler) {
            const x = chart._components.xAxis[0].config.categoryLabel[categoryIndex];
            x._handler = (e) => {
              const obj = {
                'name': 'xAxisLabel',
                'dataIndex': categoryIndex,
              };
              functionWrapper.handleClickType(obj);
            };
            x[0].addEventListener('click', x._handler);
          }
        });
        // for yAxis labels
        if (!chart._components.yAxis[0].config.axisContainer._handler) {
          const y = chart._components.yAxis[0].config.axisContainer;
          y._handler = (e) => {
            const obj = {
              'name': 'yAxisLabel',
            };
            functionWrapper.handleClickType(obj);
          };
          y[0].addEventListener('click', y._handler);
        }
        // for xaxisname and yaxisname
        if (!chart._components.xAxis[0].config.axisNameContainer._handler) {
          const x = chart._components.xAxis[0].config.axisNameContainer;
          x._handler = (e) => {
            functionWrapper.handleClickType({
              name: 'xAxisName',
              location: 'chart',
            });
          };
          x[0].addEventListener('click', x._handler);
        }
        if (!chart._components.yAxis[0].config.axisNameContainer._handler) {
          const y = chart._components.yAxis[0].config.axisNameContainer;
          y._handler = (e) => {
            functionWrapper.handleClickType({
              name: 'yAxisName',
              location: 'chart',
            });
          };
          y[0].addEventListener('click', y._handler);
        }

        // for trendline value
        function findIndexTrendLine(n) {
          let i = 0;
          const trendlines = chartInstance.getJSONData().trendlines;
          while (n - trendlines[i]['line'].length > 0) {
            n -= trendlines[i]['line'].length;
            i++;
          }
          return {
            trendValueDatasetIndex: i,
            trendValueDataIndex: n - 1,
          };
        }
        const trendLineValueGraphics = chart._components.yAxis[0]._graphics;
        for (const node in trendLineValueGraphics) {
          if (node.includes('trend') && !trendLineValueGraphics[node]._handler) {
            trendLineValueGraphics[node]._handler = (e) => {
              const n = +node.split('_')[0] + 1;
              const {trendValueDatasetIndex, trendValueDataIndex} = findIndexTrendLine(n);
              functionWrapper.handleClickType({
                name: 'trendLineValue',
                datasetIndex: trendValueDatasetIndex,
                dataIndex: trendValueDataIndex,
              });
            };
            trendLineValueGraphics[node][0].addEventListener('click', trendLineValueGraphics[node]._handler);
          }
        }
        const refLines = chart._components.canvas[0]._components.axisRefVisualCartesian[0]._graphics;
        for (const ref in refLines) {
          // trendline / trendzone
          if (ref.includes('trend') && ref.includes('axis_numeric')) {
            const node = ref.split('_')[3].substring(5);
            let i = 0;
            let index = '';
            while (0 <= +node[i] && +node[i] <= 9) {
              index += +node[i];
              i++;
            }
            if (!refLines[ref]._handler) {
              refLines[ref]._handler = (e) => {
                const {trendValueDatasetIndex, trendValueDataIndex} = findIndexTrendLine(+index + 1);
                functionWrapper.handleClickType({
                  'name': 'trendLine',
                  'trendValueDatasetIndex': trendValueDatasetIndex,
                  'trendValueDataIndex': trendValueDataIndex,
                });
              };
              refLines[ref][0].addEventListener('click', refLines[ref]._handler);
            }
          }

          // Horizontal div lines
          else if (refLines[ref].type === 'path' && ref.includes('axis_numeric')) {
            if (!refLines[ref]._handler) {
              refLines[ref]._handler = (e) => {
                functionWrapper.handleClickType({
                  'name': 'HdivLine',
                  'location': 'chart',
                });
              };
            }
            refLines[ref][0].addEventListener('click', refLines[ref]._handler);
          }

          // alternate horizontal bands
          else if (refLines[ref].type === 'rect' && ref.includes('axis_numeric')) {
            if (!refLines[ref]._handler) {
              refLines[ref]._handler = (e) => {
                functionWrapper.handleClickType({
                  'name': 'alternateHorizontalBand',
                  'location': 'chart',
                });
              };
            }
            refLines[ref][0].addEventListener('click', refLines[ref]._handler);
          }
          // Vertical div lines
          else if (refLines[ref].type === 'path' && ref.includes('axis_category') &&
          refLines[ref].parent[0].classList.value.includes('axisReferenceVisualsBottom')) {
            if (!refLines[ref]._handler) {
              refLines[ref]._handler = (e) => {
                functionWrapper.handleClickType({
                  'name': 'VdivLine',
                  'location': 'chart',
                });
              };
            }
            refLines[ref][0].addEventListener('click', refLines[ref]._handler);
          }
          // Vertical lines
          else if (refLines[ref].type === 'path' && ref.includes('axis_category') &&
          refLines[ref].parent[0].classList.value.includes('axisReferenceVisualsMiddle')) {
            if (!refLines[ref]._handler) {
              refLines[ref]._handler = (e) => {
                functionWrapper.handleClickType({
                  'name': 'VLine',
                  'location': 'chart',
                });
              };
            }
            refLines[ref][0].addEventListener('click', refLines[ref]._handler);
          }

          // alternate Vertical bands
          else if (refLines[ref].type === 'rect' && ref.includes('axis_category')) {
            if (!refLines[ref]._handler) {
              refLines[ref]._handler = (e) => {
                functionWrapper.handleClickType({
                  'name': 'alternateVerticalBand',
                  'location': 'chart',
                });
              };
            }
            refLines[ref][0].addEventListener('click', refLines[ref]._handler);
          }
        }
      }

      // for value
      chart.getDatasets().forEach((dataArray) => {
        dataArray.components.data.forEach((data, dataIndex) => {
          const graphicsLabel = data.graphics.label;
          if (graphicsLabel && !graphicsLabel._handler) {
            graphicsLabel._handler = (e) => {
              functionWrapper.handleClickType({
                name: 'plotValue',
                datasetIndex: dataArray.getJSONIndex(),
                dataIndex: dataIndex,
              });
            };
            graphicsLabel[0].addEventListener('click', graphicsLabel._handler);
          }
        });
      });
    });
    Fusioncharts.addEventListener('graphicalElementAttached', (event) => {
      const chart = event.sender;
      // for Chart
      if (chart && chart.getName() == 'background' && !chart._graphics.backgroundElement._handler) {
        const backgroundElement = chart._graphics.backgroundElement;
        backgroundElement._handler = (e) => {
          const obj = {
            'name': 'chartDS',
          };
          functionWrapper.handleClickType(obj);
        };
        backgroundElement[0].addEventListener('click', backgroundElement._handler);
      }
      // for Canvas
      if (chart && chart.getName() == 'canvas' && chart._graphics.canvasElement && !chart._graphics.canvasElement._handler) {
        const canvasElement = chart._graphics.canvasElement;
        canvasElement._handler = (e) => {
          // console.log('canvas')
          const obj = {
            'name': 'canvas',
          };
          functionWrapper.handleClickType(obj);
        };
        canvasElement[0].addEventListener('click', canvasElement._handler);
      }
      // for Caption
      if (chart && chart.getName() == 'caption' && !chart._graphics.captionElement._handler) {
        const caption = chart._graphics.captionElement;
        caption._handler = (e) => {
          // console.log('caption = ', e.target.innerHTML);
          const obj = {
            'name': 'caption',
          };
          functionWrapper.handleClickType(obj);
        };
        caption[0].addEventListener('click', caption._handler);
      }
      // for subCaption
      if (chart && chart.getName() == 'subCaption' && !chart._graphics.subCaptionElement._handler) {
        const subCaption = chart._graphics.subCaptionElement;
        subCaption._handler = (e) => {
          const obj = {
            'name': 'subCaption',
          };
          functionWrapper.handleClickType(obj);
        };
        subCaption[0].addEventListener('click', subCaption._handler);
      }
    });
  },
  name: 'click-handler',
  type: 'extension',
  requiresFusionCharts: true,
};
