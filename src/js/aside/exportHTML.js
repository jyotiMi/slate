/* eslint-disable no-multi-str */
/* eslint-disable no-unused-vars */
/* eslint-disable require-jsdoc */

import {chartObject, chart} from '../createchart';

export function exportHTML(e) {
  const type = chart.chartType();
  let width = document.getElementById('chart-width').value, height = document.getElementById('chart-height').value;

  if(!+height.trim() || !+width.trim()){
    height = window.innerHeight - 60;
    width = '100%';
  }

  const html = '<!DOCTYPE html><html><head>\
    <script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>\
    <script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script></head>\
    <body>\
    <div id="fc"></div>\
    <script>\
    FusionCharts.ready(function(){\
        var fusioncharts = new FusionCharts({\
        type:'+'"'+type+'"'+','+
        'renderAt: "fc",'+
        'width: "' + width + '",'+
        'height: "' + height + '",'+
        'dataFormat: "json",'+
        'dataSource:'+JSON.stringify(chartObject)+
        '}).render()'+
    '})'+
    '</script>\
    </body></html>';


  const blob = new Blob([html], {type: 'text/html'});
  downloadFile(blob, 'FC' + type);
}
function downloadFile(blob, filename='FusionChart') {
  const url = window.URL.createObjectURL(blob);
  if(window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, filename + '.html')
  } else {
    const a = document.createElement('a');
    a.href = url;
    a.download = filename + '.html';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}
