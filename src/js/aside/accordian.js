import {chartObject, chart, createChart, setChart, setChartObject} from '../createchart.js';
import {column2d, line, area2d, bar2d, pie2d, pie3d, column3d,mscolumn2d, mscolumn3d, msbar2d, msbar3d, bar3d} from './asideDataDesc'
import {render} from '../render'
import {filterLink} from '../filterLink'
export const functionWrapper = function() {
  let parentEvent = {};

  function resetGlobal() {
    parentEvent = {};
  }

  function createAccordion() {
    const chartType = chart.chartType();
    let type;
    if (chartType === 'column2d') {
      type = column2d;
    } else if (chartType === 'line') {
      type = line;
    } else if (chartType === 'area2d') {
      type = area2d;
    } else if (chartType === 'bar2d') {
      type = bar2d;
    } else if (chartType === 'pie2d') {
      type = pie2d;
    } else if (chartType === 'pie3d') {
      type = pie3d;
    } else if (chartType === 'column3d') {
      type = column3d;
    } else if (chartType==='mscolumn2d') {
      type=mscolumn2d;
    } else if (chartType==='msbar2d') {
      type=msbar2d;
    } else if (chartType==='msbar3d') {
      type=msbar3d;
    } else if (chartType==='mscolumn3d') {
      type=mscolumn3d;
    } else if (chartType==='bar3d') {
      type=bar3d;
    }

    const skeleton = {
      'parent': {
        'name': 'div',
        'property': {
          'class': 'aside-customize',
          'id': 'aside-customize',
        },
      },
      'children': [{
        'parent': {
          'name': 'ul',
          'property': {
            'class': 'customize-type-list',
          },
        },
        'children': (() => {
          const children = [];
          for (const part in type) {
            const dom = {
              'parent': {
                'name': 'li',
              },
              'children': [{
                'parent': {
                  'name': 'div',
                  'property': {
                    'class': 'dropdown_heading',
                  },
                  'event': function() {
                    this.addEventListener('click', toggleDropdown.bind(null, part));
                  },
                },
                'children': [{
                  'parent': {
                    'name': 'h2',
                    'property': {
                      'class': 'customize-item_' + part,
                      'id': 'customize_' + part,
                    },
                    'text': type[part]['name'].split('_')[0],
                  },
                }, {
                  'parent': {
                    'name': 'span',
                    'text': type[part]['name'].split('_')[1],
                  },
                }],
              }, {
                'parent': {
                  'name': 'ul',
                  'property': {
                    'id': part,
                    'class': 'dropdown',
                    'style': 'display: none',
                  },
                },
                'children': (function() {
                  if (part === 'dataplot' || part === 'plotValue' || part === 'xAxisLabel') {
                    return levelplotArea(type, part);
                  } else {
                    return plotArea(type, part);
                  }
                })(),
              }],
            };
            children.push(dom);
          }
          return children;
        })(),
      }],
    };
    const asideMenu = document.querySelector('#aside-menu');
    while (asideMenu.hasChildNodes()) {
      const child = asideMenu.lastChild;
      asideMenu.removeChild(child);
    }
    asideMenu.appendChild(render(skeleton));
  }

  function toggleDropdown(part) {
    const dropdown = document.getElementById(part);
    if (dropdown.style.display === 'none') {
      // close others and open this one
      dropdown.style.display = 'block';
      const parent = dropdown.parentElement.parentElement;
      const children = parent.children;

      for (let index = 0; index < children.length; index++) {
        const id = children[index].children[1].getAttribute('id');
        if (id !== part) {
          children[index].children[1].style.display = 'none';
        }
      }
      if (part === 'dataplot' || part === 'plotValue' || part === 'xAxisLabel') {
        const dom = document.getElementsByClassName(part + '_' + 'chart')[0];
        dom.click();
      }
    } else {
      dropdown.style.display = 'none';
    }
  }

  function levelplotArea(type, part) {
    const children = [];
    const levelContainer = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'level-container',
        },
      },
      'children': createLevelSkeleton(type, part),
    };
    const attr_list = {
      'parent': {
        'name': 'ul',
        'property': {
          'class': 'attr-container_' + part,
        },
      },
    };
    children.push(levelContainer);
    children.push(attr_list);
    return children;
  }

  function createLevelSkeleton(type, part) {
    const children = [];

    const levels = getLevels(part);
    for (let i = 0; i < levels.length; i++) {
      const skeleton = {
        'parent': {
          'name': 'a',
          'property': {
            'class': part + '_' + levels[i],
          },
          'text': levels[i],
          'event': function() {
            if (levels[i] === 'series') {
              if (isMultiseries()) {
                this.addEventListener('click', createFeatures.bind(null, type, part, levels[i]));
              } else {
                this.style.cursor = 'default';
                this.addEventListener('click', function(event) {
                  event.preventDefault();
                });
              }
            } else {
              this.addEventListener('click', createFeatures.bind(null, type, part, levels[i]));
            }
          },
        },
      };
      children.push(skeleton);
    }
    return children;
  }

  function isMultiseries() {
    const type = chart.chartType();
    if (type.substring(0, 2) === 'ms') {
      return true;
    }
    return false;
  }

  function getLevels(part) {
    const levelDict = {
      'dataplot': ['chart', 'data', 'series'],
      'plotValue': ['chart', 'data', 'series'],
      'trendline': ['chart', 'line'],
      'xAxisLabel': ['chart', 'data'],
    };
    return levelDict[part];
  }

  function plotArea(type, part) {
    // const dom = document.querySelector('#' + part)
    // while (dom.hasChildNodes()) {
    //     dom.removeChild(dom.lastChild)
    // }
    const children = [];
    for (const property in type[part]['properties']) {
      const skeleton = {
        'parent': {
          'name': 'li',
        },
        'children': [{
          'parent': {
            'name': 'h2',
            'property': {
              'class': 'input-heading',
            },
            'text': property,
          },
        },
        {
          'parent': {
            'name': 'ul',
          },
          'children': (() => {
            const children = [];
            const values = type[part]['properties'][property];
            for (const value of values) {
              if (value['inputFieldType'].toLowerCase() === 'text') {
                children.push(inputText(value));
              } else if (value['inputFieldType'].toLowerCase() === 'number') {
                children.push(inputNumber(value));
              } else if (value['inputFieldType'].toLowerCase() === 'select') {
                children.push(inputDropDown(value));
              } else if (value['inputFieldType'].toLowerCase() === 'range') {
                children.push(inputRange(value));
              } else if (value['inputFieldType'].toLowerCase() === 'color') {
                children.push(inputColor(value));
              } else if (value['inputFieldType'].toLowerCase() === 'checkbox') {
                children.push(inputCheckBox(value));
              } else if (value['inputFieldType'].toLowerCase() === 'url') {
                children.push(inputURL(value));
              }
            }
            return children;
          })(),
        },
        ],
      };
      // dom.appendChild(render(skeleton))
      children.push(skeleton);
    }
    return children;
  }

  function createFeatures(type, part, level) {
    const clickedLevel = document.getElementsByClassName(part + '_' + level)[0];
    const parent = clickedLevel.parentElement;
    let node = parent.firstChild;
    while (node) {
      if (node !== clickedLevel) {
        node.style.backgroundColor = '#474954';
      }
      node = node.nextSibling;
    }

    clickedLevel.style.backgroundColor = '#333542';

    const dom = document.getElementsByClassName('attr-container_' + part);
    // remove previous child
    while (dom[0].hasChildNodes()) {
      dom[0].removeChild(dom[0].lastChild);
    }
    if (level === 'data' || level === 'series' || level ==='line') {
      if (Object.keys(parentEvent).length === 0) {
        const text = 'Note: Please click on ' + level + ' to see the changes';
        const note = {
          'parent': {
            'name': 'div',
            'text': text,
          },
        };
        dom[0].appendChild(render(note));
      } else {
        const children = createFeaturesBox(type[part], level);
        dom[0].appendChild(render(children));
      }
    } else {
      const children = createFeaturesBox(type[part], level);
      dom[0].appendChild(render(children));
    }
  }

  function handleClickType(eventDetails) {
    if (['dataplot', 'plotValue', 'xAxisLabel', 'trendLine'].includes(eventDetails['name'])) {
      parentEvent = eventDetails;
    }

    const part = eventDetails['name'];
    const dom = document.getElementById('customize_' + part);
    if (dom) {
      const sliderBtn = document.getElementsByClassName('fa-sliders');
      if (sliderBtn[0]) {
        const clickAsideBtn = document.getElementById('btn-open-aside');
        clickAsideBtn.click();
      }
      dom.click();
    }
  }

  function createFeaturesBox(part, level) {
    const values = part['properties'][level];
    const skeleton = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'features-box',
        },
      },
      'children': [
        {
          'parent': {
            'name': 'ul',
          },
          'children': (() => {
            const children = [];
            // const values = type[part]['properties'][property]
            for (const value of values) {
              if (value['inputFieldType'].toLowerCase() === 'text') {
                children.push(inputText(value));
              } else if (value['inputFieldType'].toLowerCase() === 'number') {
                children.push(inputNumber(value));
              } else if (value['inputFieldType'].toLowerCase() === 'select') {
                children.push(inputDropDown(value));
              } else if (value['inputFieldType'].toLowerCase() === 'range') {
                children.push(inputRange(value));
              } else if (value['inputFieldType'].toLowerCase() === 'color') {
                children.push(inputColor(value));
              } else if (value['inputFieldType'].toLowerCase() === 'checkbox') {
                children.push(inputCheckBox(value));
              } else if (value['inputFieldType'].toLowerCase()==='url') {
                children.push(inputURL(value));
              }
            }
            return children;
          })(),
        },
      ],
    };
    return skeleton;
  }

  function handleChartChanges() {
    const value = arguments[0];
    const event = arguments[1];
    function setAttr(label, value) {
      const temp = JSON.parse(JSON.stringify(chartObject))
      temp['chart'][label] = value;
      setChartObject(temp)
      if (label !== 'clickURL') {
        chart.setChartAttribute(label, value);
      }
    }
    if (value['location'] === 'chart') {
      const chartData = chartObject;
      // let chartData = chart.getJSONData()
      // remove series and data level dependencies
      if (chartData.dataset) { // multiseries
        // find data and series level attribute
        const data_attr = getConflictingAttributes('chart', value['id'].split('_')[1], 'data');
        const series_attr = getConflictingAttributes('chart', value['id'].split('_')[1], 'series');

        // loop over chartdata.dataset , remove seriesAttr

        for (let i = 0; i < chartData.dataset.length; i++) {
          delete chartData.dataset[i][series_attr];
          // loop over chartdata.dataset[index].data
          for (let j = 0; j < chartData.dataset[i].data.length; j++) {
            // remove dataattr
            delete chartData.dataset[i].data[j][data_attr];
          }
        }
      } else {
        // find conflicting attribute
        const data_attr = getConflictingAttributes('chart', value['id'].split('_')[1], 'data');
        // loop over chartData.data
        for (let i = 0; i < chartData.data.length; i++) {
          // remove dataAttr
          delete chartData.data[i][data_attr];
        }
      }
      chart.setJSONData(filterLink());
      if (value['inputFieldType'] === 'checkbox') {
        setAttr(value['id'].split('_')[1], event.target.checked ? '1' : '0');
      } else {
        setAttr(value['id'].split('_')[1], event.target.value);
      }
    } else if (value['location'] === 'data') {
      // take json data
      // update JSON data
      // set chart data to updated JSON
      const chartData = chartObject;
      const dataIndex = parentEvent['dataIndex'];
      const datasetIndex = parentEvent['datasetIndex'];
      const data_property = value['id'].split('_')[1];

      if (value['inputFieldType'] === 'checkbox') {
        if (chartData.dataset) {
          chartData.dataset[datasetIndex].data[dataIndex][data_property] = (event.target.checked ? '1' : '0');
        } else {
          chartData.data[dataIndex][data_property] = (event.target.checked ? '1' : '0');
        }
      } else {
        if (chartData.dataset) {
          chartData.dataset[datasetIndex].data[dataIndex][data_property] = event.target.value;
        } else {
          chartData.data[dataIndex][data_property] = event.target.value;
        }
      }
      // chart.setJSONData(chartData)
      chart.setJSONData(filterLink());
    } else if (value['location'] === 'series') {
      const chartData = chartObject;

      // remove data related conflicting attributes
      const data_attr = getConflictingAttributes('series', value['id'].split('_')[1], 'data');
      const datasetIndex = parentEvent['datasetIndex'];
      // eslint-disable-next-line no-unused-vars
      const dataIndex = parentEvent['dataIndex'];
      // loop over chartData.data
      for (let i = 0; i < chartData.dataset[datasetIndex].data.length; i++) {
        // remove chartdata.data[index].dataAttr
        delete chartData.dataset[datasetIndex].data[i][data_attr];
      }

      const data_property = value['id'].split('_')[1];
      if (value['inputFieldType'] === 'checkbox') {
        chartData.dataset[datasetIndex][data_property] = (event.target.checked ? '1' : '0');
      } else {
        chartData.dataset[datasetIndex][data_property] = event.target.value;
      }
      // chart.setJSONData(chartData)
      chart.setJSONData(filterLink());
    } else if (value['location'] === 'line') {
      // get line index from parentEvent
      // apply attribute on that line index

      const line_index = parentEvent['line_index'];
      const chartData = chartObject;
      const data_property = value['id'].split('_')[1];
      if (value['inputFieldType'] === 'checkbox') {
        chartData.trendlines[0].line[line_index][data_property] = (event.target.checked ? '1' : '0');
      } else {
        chartData.trendlines[0].line[line_index][data_property] = event.target.value;
      }
      chart.setJSONData(filterLink());
    } else {
      console.log('please specify location');
    }
  }

  function inputCheckBox(value) {
    const skeleton = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'input',
        },
      },
      'children': [{
        'parent': {
          'name': 'div',
        },
        'children': [{
          'parent': createLabel(value),
        },
        {
          'parent': {
            'name': 'input',
            'property': (() => {
              const skeleton = {
                'class': 'input-checkbox',
                'id': value['id'],
                'type': 'checkbox',
              };
              if (value['checked'] === 0) {
                // skeleton['checked'] = will either have current value or will have
                // default value
              }
              if (+value['defaultActive'] === 0 && !skeleton['value']) {
                skeleton['disabled'] = '';
              }
              if (value['willActivate']) {
                value.willActivate();
              }
              return skeleton;
            })(),
            'event': function() {
              this.addEventListener('change', handleChartChanges.bind(null, value));
            },
          },
        }, {
          'parent': {
            'name': 'span',
            'property': {
              'class': 'info_btn',
              'style': 'display: block;cursor: pointer',
            },
            'event': function() {
              const domId = 'info_' + value['id'].split('_')[1];
              this.addEventListener('click', openAttributeInfo.bind(null, value, domId));
            },
          },
          'children': [{
            'parent': {
              'name': 'i',
              'property': {
                'class': 'fa fa-info',
              },
            },
          }],
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'attr_info',
              'id': 'info_' + value['id'].split('_')[1],
              'style': 'display: none',
            },
          },
        }],
      },
      ],
    };
    addNote(value, skeleton);
    return skeleton;
  }

  function inputText(value) {
    const skeleton = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'input',
        },
      },
      'children': [{
        'parent': {
          'name': 'div',
        },
        'children': [{
          'parent': createLabel(value),
        }, {
          'parent': {
            'name': 'input',
            'property': (() => {
              const skeleton = {
                'class': 'input-text',
                'id': value['id'],
                'type': 'text',
              };
              if (value['placeholder'].length !== 0) {
                skeleton['placeholder'] = value['placeholder'];
              }
              if (value['value'] === 0) {
                // skeleton['value'] = will either have current value or will have
                // default value
              }
              if (+value['defaultActive'] === 0 && !skeleton['value']) {
                skeleton['disabled'] = '';
              }
              if (value['willActivate']) {
                value.willActivate();
              }
              return skeleton;
            })(),
            'event': function() {
              this.addEventListener('input', handleChartChanges.bind(null, value));
            },
          },
        }, {
          'parent': {
            'name': 'span',
            'property': {
              'class': 'info_btn',
              'style': 'display: block;cursor: pointer',
            },
            'event': function() {
              const domId = 'info_' + value['id'].split('_')[1];
              this.addEventListener('click', openAttributeInfo.bind(null, value, domId));
            },
          },
          'children': [{
            'parent': {
              'name': 'i',
              'property': {
                'class': 'fa fa-info',
              },
            },
          }],
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'attr_info',
              'id': 'info_' + value['id'].split('_')[1],
              'style': 'display: none',
            },
          },
        }],
      }],
    };
    addNote(value, skeleton);
    return skeleton;
  }
  function getCompatibleEventName() {
    return window.navigator.userAgent.toLowerCase().indexOf('trident') >= 0 ? 'change' : 'input';
  }
  function inputDropDown(value) {
    const skeleton = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'input',
        },
      },
      'children': [{
        'parent': {
          'name': 'div',
        },
        'children': [{
          'parent': createLabel(value),
        }, {
          'parent': {
            'name': 'select',
            'property': (() => {
              const skeleton = {
                'class': 'input-select',
              };
              if (value['value'] === 0) {
                // skeleton['value'] = will either have current value or will have
                // default value
              }
              if (+value['defaultActive'] === 0 && !skeleton['value']) {
                skeleton['disabled'] = '';
              }
              if (value['willActivate']) {
                value.willActivate();
              }
              return skeleton;
            })(),
            'event': function() {
              this.addEventListener(getCompatibleEventName(), handleChartChanges.bind(null, value));
            },
          },
          'children': (() => {
            const children = [];
            if (Array.isArray(value['selectValues'])) {
              for (const option of value['selectValues']) {
                children.push({
                  'parent': {
                    'name': 'option',
                    'property': {
                      'class': 'input-option',
                      'value': option,
                    },
                    'text': option,
                  },
                });
              }
            } else {
              // if it is an object
              for (const option in value['selectValues']) {
                children.push({
                  'parent': {
                    'name': 'option',
                    'property': {
                      'class': 'input-option',
                      'value': value['selectValues'][option],
                    },
                    'text': option,
                  },
                });
              }
            }
            return children;
          })(),

          // adding event listener to dropdown

        }, {
          'parent': {
            'name': 'span',
            'property': {
              'class': 'info_btn',
              'style': 'display: block;cursor: pointer',
            },
            'event': function() {
              const domId = 'info_' + value['id'].split('_')[1];
              this.addEventListener('click', openAttributeInfo.bind(null, value, domId));
            },
          },
          'children': [{
            'parent': {
              'name': 'i',
              'property': {
                'class': 'fa fa-info',
              },
            },
          }],
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'attr_info',
              'id': 'info_' + value['id'].split('_')[1],
              'style': 'display: none',
            },
          },
        }],
      }],
    };
    addNote(value, skeleton);
    return skeleton;
  }

  function inputColor(value) {
    let parentOfColorPicker;
    const skeleton = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'input',
        },
      },
      'children': [{
        'parent': {
          'name': 'div',
        },
        'children': [{
          'parent': createLabel(value),
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'flex-row',
            },
            'event': function() {
              parentOfColorPicker = this;
            },
          },
          'children': [{
            'parent': {
              'name': 'input',
              'property': (() => {
                const skeleton = {
                  'class': 'input-color',
                  'id': value['id'],
                };
                if (value['value'] === 0) {
                  // skeleton['value'] = will either have current value or will have
                  // default value
                }
                if (+value['defaultActive'] === 0 && !skeleton['value']) {
                  skeleton['disabled'] = '';
                }
                if (value['willActivate']) {
                  value.willActivate();
                }
                return skeleton;
              })(),
              'event': function() {
                this.addEventListener('input', handleChartChanges.bind(null, value));
                // create an observer instance
                const observer = new MutationObserver(function(mutations) {
                  mutations.forEach(function(mutation) {
                    if (mutation.type === 'attributes') {
                      // console.log(mutation.target.value)
                      handleChartChanges(value, mutation);
                    }
                    // or do whatever you want here
                  });
                });
                // configuration of the observer:
                const config = {attributes: true, characterData: true, attributeFilter: ['value']};
                // pass in the target node, as well as the observer options
                observer.observe(this, config);
              },
            },
          }, {
            'parent': {
              'name': 'span',
              'event': function() {
                const self = this;
                new Promise(function(resolve, reject) {
                  setTimeout(() => {
                    const pickr = Pickr.create({
                      el: self,
                      theme: 'nano',
                      default: '#0000ff',
                      container: parentOfColorPicker,
                      swatches: [
                        'rgba(244, 67, 54, 1)',
                        'rgba(233, 30, 99, 0.95)',
                        'rgba(156, 39, 176, 0.9)',
                        'rgba(103, 58, 183, 0.85)',
                        'rgba(63, 81, 181, 0.8)',
                        'rgba(33, 150, 243, 0.75)',
                      ],

                      components: {
                        // Main components
                        preview: true,
                        opacity: true,
                        hue: true,

                        // Input / output Options
                        interaction: {
                          hex: true,
                          rgba: true,
                          hsla: true,
                          hsva: true,
                          cmyk: true,
                          input: true,
                          clear: false,
                          save: true,
                        },
                      },
                    });

                    function changeColor(e) {
                      const element = pickr.options.container.childNodes[0];
                      const value = e.toHEXA().toString();
                      if (!element.disabled) {
                        if (element.value.length !== 0) {
                          element.value += ',' + value;
                          element.setAttribute('value', element.value+',' + value);
                        } else {
                          element.value = value;
                          element.setAttribute('value', value);
                        }
                      }
                    }

                    function handleSaveNClear(e) {
                      changeColor(e);
                      pickr.hide();
                    }
                    pickr.on('save', handleSaveNClear);
                    pickr.on('clear', handleSaveNClear);
                  }, 200);
                });
              },
            },
          }],
        }, {
          'parent': {
            'name': 'span',
            'property': {
              'class': 'info_btn',
              'style': 'display: block;cursor: pointer',
            },
            'event': function() {
              const domId = 'info_' + value['id'].split('_')[1];
              this.addEventListener('click', openAttributeInfo.bind(null, value, domId));
            },
          },
          'children': [{
            'parent': {
              'name': 'i',
              'property': {
                'class': 'fa fa-info',
              },
            },
          }],
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'attr_info',
              'id': 'info_' + value['id'].split('_')[1],
              'style': 'display: none',
            },
          },
        }],
      }],
    };
    addNote(value, skeleton);
    return skeleton;
  }

  function openAttributeInfo() {
    // debugger
    const value = arguments[0];
    const domId = arguments[1];
    const dom = document.getElementById(domId);

    if (dom.style.display === 'none') {
      dom.style.display = 'block';
      if (value['description']) {
        dom.innerHTML = value['description'];
      } else {
        dom.innerHTML = 'No description';
      }
    } else {
      dom.style.display = 'none';
    }
  }

  function inputRange(value) {
    const skeleton = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'input',
        },
      },
      'children': [{
        'parent': {
          'name': 'div',
        },
        'children': [{
          'parent': createLabel(value),
        },
        {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'flex-row',
            },
          },
          'children': [{
            'parent': {
              'name': 'input',
              'property': (() => {
                const skeleton = {
                  'class': 'input-range',
                  'id': value['id'],
                  'type': 'range',
                  'min': value['min'],
                  'max': value['max'],
                };
                if (value['value'] === 0) {
                  // skeleton['value'] = will either have current value or will have
                  // default value
                }
                if (+value['defaultActive'] === 0 && !skeleton['value']) {
                  skeleton['disabled'] = '';
                }
                if (value['willActivate']) {
                  value.willActivate();
                }
                return skeleton;
              })(),
              'event': function() {
                this.addEventListener(getCompatibleEventName(), handleChartChanges.bind(null, value));
                this.addEventListener(getCompatibleEventName(), (e) => {
                  this.nextSibling.innerHTML = e.target.value;
                });
              },
            },
          }, {
            'parent': {
              'name': 'span',
              'property': {
                'class': 'number',
              },
              'text': value['value'],
            },
          }],
        }, {
          'parent': {
            'name': 'span',
            'property': {
              'class': 'info_btn',
              'style': 'display: block;cursor: pointer',
            },
            'event': function() {
              const domId = 'info_' + value['id'].split('_')[1];
              this.addEventListener('click', openAttributeInfo.bind(null, value, domId));
            },
          },
          'children': [{
            'parent': {
              'name': 'i',
              'property': {
                'class': 'fa fa-info',
              },
            },
          }],
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'attr_info',
              'id': 'info_' + value['id'].split('_')[1],
              'style': 'display: none',
            },
          },
        },
        ],
      }],
    };
    addNote(value, skeleton);
    return skeleton;
  }
  function inputURL(value) {
    const skeleton= {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'input',
        },
      },
      'children': [{
        'parent': {
          'name': 'div',
        },
        'children': [{
          'parent': createLabel(value),
        }, {
          'parent': {
            'name': 'input',
            'property': (() => {
              const skeleton = {
                'class': 'input-text',
                'id': value['id'],
                'type': 'url',
              };
              if (value['placeholder'].length !== 0) {
                skeleton['placeholder'] = value['placeholder'];
              }
              if (value['value'] === 0) {
                // skeleton['value'] = will either have current value or will have
                // default value
              }
              if (+value['defaultActive'] === 0 && !skeleton['value']) {
                skeleton['disabled'] = '';
              }
              if (value['willActivate']) {
                value.willActivate();
              }
              return skeleton;
            })(),
            'event': function() {
              // this.addEventListener('input', (e) =>console.log(e.target.value))
              this.addEventListener('input', handleChartChanges.bind(null, value));
            },
          },
        }, {
          'parent': {
            'name': 'span',
            'property': {
              'class': 'info_btn',
              'style': 'display: block;cursor: pointer',
            },
            'event': function() {
              const domId = 'info_' + value['id'].split('_')[1];
              this.addEventListener('click', openAttributeInfo.bind(null, value, domId));
            },
          },
          'children': [{
            'parent': {
              'name': 'i',
              'property': {
                'class': 'fa fa-info',
              },
            },
          }],
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'attr_info',
              'id': 'info_' + value['id'].split('_')[1],
              'style': 'display: none',
            },
          },
        }],
      }],
    };
    addNote(value, skeleton);
    return skeleton;
  }

  function inputNumber(value) {
    const skeleton = {
      'parent': {
        'name': 'li',
        'property': {
          'class': 'input',
        },
      },
      'children': [{
        'parent': {
          'name': 'div',
        },
        'children': [{
          'parent': createLabel(value),
        }, {
          'parent': {
            'name': 'input',
            'property': (() => {
              const skeleton = {
                'class': 'input-number',
                'id': value['id'],
                'type': 'number',
              };
              if (value['placeholder'].length !== 0) {
                skeleton['placeholder'] = value['placeholder'];
              }
              if (value['value'] === 0) {
                // skeleton['value'] = will either have current value or will have
                // default value
              }
              if (+value['defaultActive'] === 0 && !skeleton['value']) {
                skeleton['disabled'] = '';
              }
              if (value['willActivate']) {
                value.willActivate();
              }
              return skeleton;
            })(),
            'event': function() {
              this.addEventListener('input', handleChartChanges.bind(null, value));
            },
          },
        }, {
          'parent': {
            'name': 'span',
            'property': {
              'class': 'info_btn',
              'style': 'display: block;cursor: pointer',
            },
            'event': function() {
              const domId = 'info_' + value['id'].split('_')[1];
              this.addEventListener('click', openAttributeInfo.bind(null, value, domId));
            },
          },
          'children': [{
            'parent': {
              'name': 'i',
              'property': {
                'class': 'fa fa-info',
              },
            },
          }],
        }, {
          'parent': {
            'name': 'div',
            'property': {
              'class': 'attr_info',
              'id': 'info_' + value['id'].split('_')[1],
              'style': 'display: none',
            },
          },
        }],
      }],
    };
    addNote(value, skeleton);
    return skeleton;
  }

  function getConflictingAttributes(currentLevel, attribute, lowerLevel) {
    const dict = {
      chart: ['paletteColors', 'plotFillAlpha', 'plotBorderDashed', 'plotFillRatio', 'plotFillHoverColor', 'plotFillHoverAlpha', 'plotHoverGradientColor', 'plotHoverRatio', 'plotHoverAngle'],
      series: ['color', 'alpha', 'dashed', 'ratio', 'hoverColor', 'hoverAlpha', 'hoverGradientColor', 'hoverRatio', 'hoverAngle'],
      data: ['color', 'alpha', 'dashed', 'ratio', 'hoverColor', 'hoverAlpha', 'hoverGradientColor', 'hoverRatio', 'hoverAngle'],
    };

    const levelArr = dict[currentLevel];
    const index = levelArr.indexOf(attribute);
    return dict[lowerLevel][index];
  }

  // eslint-disable-next-line no-unused-vars
  function checkActive(e) {
    const children = Array.from(e.target.parentElement.children);
    for (const child of children) {
      if (child.classList.contains('active-customize')) {
        child.classList.remove('active-customize');
        break;
      }
    }
    e.target.classList.add('active-customize');
  }

  function addNote(value, skeleton) {
    if (value['note']) {
      skeleton['children'].push({
        'parent': {
          'name': 'p',
          'property': {
            'class': 'input-note',
          },
          'text': value['note'],
        },
      });
    }
  }

  function createLabel(value) {
    return {
      'name': 'label',
      'text': value['label'],
      'property': {
        'class': 'input-label',
        'for': value['id'],
      },
    };
  }

  return {
    createAccordion: createAccordion,
    handleClickType: handleClickType,
    resetGlobal: resetGlobal,
  };
}();
