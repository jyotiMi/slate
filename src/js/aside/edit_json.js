import {render} from '../render'
import {chartObject, chart, createChart, setChart, setChartObject} from '../createchart.js';
import {filterLink} from '../filterLink'
export function editJson() {
  const skeleton = {
    'parent': {
      'name': 'div',
      'property': {
        'id': 'json',
        'class': 'json',
      },
    },
    'children': [{
      'parent': {
        'name': 'textarea',
        'property': {
          'class': 'json-textarea',
          'id': 'json-textarea',
          'rows': '10',
          'cols': '70',
        },
        'event': function() {
          const p = document.getElementById('aside-menu');
          this.style.cssText = 'height:' + p.offsetHeight + 'px;';

          this.value = JSON.stringify(chartObject, null, 2);

          this.addEventListener('input', (e) => {
            try {
              setChartObject(JSON.parse(e.target.value));
              chart.setJSONData(filterLink());
            } catch (e) {

            }
          });
        },
      },
    }],
  };
  document.getElementById('aside-menu').appendChild(render(skeleton));
}
