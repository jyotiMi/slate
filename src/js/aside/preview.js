/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
function _createFrame() {
  const dlgWraper = document.createElement('div');
  const dlg = document.createElement('div');
  const nav = document.createElement('div');
  const btnClose = document.createElement('button');
  const tblContainer = document.createElement('div');
  const colorDiv = document.createElement('div');
  const fileName = document.createElement('div');
  const fileNameTxt = document.createElement('span');
  const pallets = ['#007bff36', '#ff2d575d', '#ffbc2d5d', '#57ff2d2f'];
  const tbl = document.createElement('table');
  dlgWraper.className = 'spreadsheet-wraper';
  dlg.className = 'spreadsheet-container';
  tblContainer.className = 'tbl-container';
  btnClose.className = 'btn btn-close';
  colorDiv.className = 'pallets';

  let ico = document.createElement('span');
  ico.className = 'fa fa-remove';
  btnClose.appendChild(ico);

  tbl.id = 'tbl-csv';
  tbl.className = 'tbl';
  btnClose.onclick = () => {
    dlgWraper.style = 'display: none';
    dlg.style = 'display:none';
  };


  nav.className = 'nav clearfix';
  fileName.className = 'filename';
  fileNameTxt.id = 'filename-text';
  fileNameTxt.innerHTML = '&nbsp;' + document.getElementById('fileText').innerHTML;
  ico = document.createElement('span');
  ico.className = 'fa fa-file';
  fileName.appendChild(ico);
  fileName.appendChild(fileNameTxt);
  nav.appendChild(fileName);
  nav.appendChild(btnClose);
  dlg.appendChild(nav);
  tblContainer.appendChild(tbl);
  dlg.appendChild(tblContainer);
  pallets.forEach((color, idx) => {
    const span = document.createElement('span');
    span.className = 'colorhint';
    span.style = 'background-color:' + color + ';';
    if (idx === 0) {
      span.innerHTML = 'X';
    } else {
      span.innerHTML = 'Y';
    }
    colorDiv.appendChild(span);
  });
  dlg.appendChild(colorDiv);
  dlgWraper.appendChild(dlg);
  document.body.appendChild(dlgWraper);
  return tbl;
}

function _fillTable(tbl, csv) {
  let tr; let td; let cellTag = 'th'; const dictIdx = []; const pallets = 3;
  tbl.innerHTML = '';
  document.getElementById('filename-text').innerHTML = '&nbsp;' + document.getElementById('fileText').innerHTML;
  csv.forEach((line, lineNo) => {
    tr = document.createElement('tr');

    if (lineNo > 0) {
      cellTag = 'td';
    }

    td = document.createElement(cellTag);
    td.className = 'row-idx';

    td.innerHTML = lineNo > 0 ? lineNo : 'SL';

    tr.appendChild(td);

    line.forEach((col, colIdx) => {
      td = document.createElement(cellTag);
      if (lineNo === 0) {
        dictIdx.push(col);
      }

      if (dictIdx.indexOf(selectedX) === colIdx) {
        td.className = 'selected-x';
      }
      if (selectedY.map((s) => dictIdx.indexOf(s)).includes(colIdx)) {
        td.className = 'selected-y-' + (colIdx % pallets);
      }
      td.innerHTML = col;
      tr.appendChild(td);
    });
    tbl.appendChild(tr);
  });
}

function previewCSV() {
  let tbl = document.getElementById('tbl-csv');
  if (!tbl) {
    tbl = _createFrame();
  } else {
    tbl.parentNode.parentNode.removeAttribute('style');
    tbl.parentNode.parentNode.parentNode.removeAttribute('style');
  }
  if (datacsv !== undefined) {
    _fillTable(tbl, data);
  }
}
