# Slate
Slate is a live FusionCharts editor

## Usage Guide

### Installation Guide

```bash 
git clone git+https://jyotiMi@bitbucket.org/jyotiMi/slate.git
npm install
npm start
```


** Create your first chart **

- Upload a CSV file
- Select the chart type
- Choose columns as x axis and y axis 
- Click on the chart button to render the chart with selected data
- CLick anywhere on the chart and naviagate over the options to customize the chart
- Click on the download button to get the chart as html


